import requests
import json
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

def get_picture_url(city, state):
    url = f"https://api.pexels.com/v1/search?query={city},{state}",
#Use requests to get a fun fact
    headers = {"AUTHORIZATION": PEXELS_API_KEY}
    r = requests.get(
        url=url,
        headers=headers
        )
    content = json.loads(r.content)

#create a dictionary of data to use containing
    picture = {
        "picture_url": content["photos"][0]["url"],
        }


#return the dictionary
    return picture


def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}",

    r = requests.get(
        url=url,
    )
    content = json.loads(r.content)

    location = {
        "lat": content["lat"],
        "lon": content["lon"],
    }

    return location.get["lat"], location.get["lon"]

def get_weather(latitude, longitude):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    r = requests.get(url=url)
    content = json.loads(r.content)
    if content:
        weather = {
            "temp": content["main"]["temp"],
            "description": content["weather"][0]["description"],
        }
        return weather
    else:
        return None
